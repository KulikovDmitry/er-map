# -*- coding: utf-8 -*-
"""Application assets."""
from flask_assets import Bundle, Environment

css = Bundle(
    'libs/bootstrap/dist/css/bootstrap.css',
    'libs/leaflet/dist/leaflet.css',
    'libs/leaflet.fullscreen/Control.FullScreen.css',
    'libs/leaflet.markercluster/dist/MarkerCluster.css',
    'libs/leaflet.markercluster/dist/MarkerCluster.Default.css',
    'libs/Leaflet.awesome-markers/dist/leaflet.awesome-markers.css',
    'libs/sidebar-v2/css/leaflet-sidebar.min.css',
    'libs/angular-carousel/dist/angular-carousel.min.css',
    'libs/angular-material/angular-material.min.css',
    'libs/leaflet.locatecontrol/dist/L.Control.Locate.min.css',
    'libs/Leaflet.EasyButton/src/easy-button.css',

    'css/style.css',
    filters='cssmin',
    output='public/css/common.css'
)

js = Bundle(
    'libs/jQuery/dist/jquery.js',
    'libs/bootstrap/dist/js/bootstrap.js',

    # AngularJS
    'libs/angular/angular.min.js',
    'libs/angular-route/angular-route.min.js',

    # AngularJS-Bootstrap
    'libs/angular-bootstrap/ui-bootstrap-tpls.min.js',
    'libs/angular-bootstrap/ui-bootstrap.min.js',

    # Leaflet
    'libs/leaflet/dist/leaflet.js',

    # Leaflet and AngularJS
    'libs/angular-simple-logger/dist/angular-simple-logger.light.min.js',
    'libs/ui-leaflet/dist/ui-leaflet.min.js',

    # FullScreen control
    'libs/leaflet.fullscreen/Control.FullScreen.js',

    # Current locate control
    'libs/leaflet.locatecontrol/dist/L.Control.Locate.min.js',

    # Marker cluster
    'libs/leaflet.markercluster/dist/leaflet.markercluster.js',

    # Sidebar
    'libs/sidebar-v2/js/leaflet-sidebar.min.js',

    # Yandex and Google tiles
    'libs/leaflet-plugins/layer/tile/Yandex.js',
    'libs/leaflet-plugins/layer/tile/Google.js',

    # Upload Images
    'libs/ng-file-upload/ng-file-upload-shim.min.js',
    'libs/ng-file-upload/ng-file-upload.min.js',

    # View images
    'libs/angular-touch/angular-touch.min.js',
    'libs/angular-carousel/dist/angular-carousel.min.js',

    # Awesome markers
    'libs/Leaflet.awesome-markers/dist/leaflet.awesome-markers.js',

    # Angular Material
    'libs/angular-material/angular-material.min.js',
    'libs/angular-aria/angular-aria.min.js',
    'libs/angular-animate/angular-animate.min.js',

    # Moment.js и moment-range.js
    'libs/moment/min/moment-with-locales.min.js',
    'libs/moment/locale/ru.js',
    'libs/moment-range/dist/moment-range.min.js',

    # Media query
    'libs/angular-media-queries/match-media.js',

    # EasyButton
    'libs/Leaflet.EasyButton/src/easy-button.js',

    # Код AgularJS приложения
    'js/app.js',
    'js/controllers.js',
    'js/services.js',
    'js/providers.js',
    'js/filters.js',
    'js/controllers/map.js',
    'js/controllers/marker.js',
    'js/controllers/alert.js',
    'js/controllers/filters.js',
    'js/controllers/report.js',
    'js/services/data.js',
    'js/services/user.js',
    'js/services/geo.js',
    'js/services/filters.js',
    'js/providers/parties.js',
    'js/providers/elections.js',
    'js/providers/groups.js',
    'js/filters/parties.js',
    'js/filters/elections.js',
    'js/filters/groups.js',
    'js/filters/types.js',

    'js/plugins.js',
    filters='jsmin',
    output='public/js/common.js'
)

assets = Environment()

assets.register('js_all', js)
assets.register('css_all', css)
