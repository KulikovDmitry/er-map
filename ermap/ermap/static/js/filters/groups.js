'use strict';

/* groups */

appFilters.filter("group_as_string", function() {
    return function(input) {
        if (!input) {
            return input;
        };
        if (input === "all") {return null;};
        var groups = input.split('.');
        return groups.join(', ');
    };
});