'use strict';

/* parties */

appFilters.filter("party_as_string", function(parties) {
    return function(input) {
        if (!input) {
            return input;
        };
        for (var i = 0; i < parties.length; i++) {
            var party = parties[i];
            if (String(party.value) === input) {
                return party.name;
            };
        };
    };
});