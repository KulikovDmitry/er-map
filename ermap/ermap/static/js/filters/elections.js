'use strict';

/* parties */

appFilters.filter("election_as_string", function(elections) {
    return function(input) {
        if (!input) {
            return input;
        };
        var elections_str = [];
        for (var i = 0; i < elections.length; i++) {
            var election = elections[i];
            if (election.value & parseInt(input)) {
                elections_str.push(election.name);
            };
        };
        return elections_str.join(', ');
    };
});