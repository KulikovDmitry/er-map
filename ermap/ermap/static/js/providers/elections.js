'use strict';

/* elections */

appProviders.provider("elections", function electionProvider() {
    var elections = [
        {value: 2, name: "ГД"},
        {value: 4, name: "ЗС"},

        {value: 1, name: "Неизвестно"},
    ];

    this.$get = function(){
        return elections;
    };
});