'use strict';

/* parties */

appProviders.provider("parties", function partyProvider() {
    var parties = [
        {value: 2, name: "Единая Россия"},
        {value: 4, name: "КПРФ"},
        {value: 8, name: "ЛДПР"},
        {value: 16, name: "Справедливая Россия"},

        {value: 1, name: "Другое"},
        // {value: 1 << 30, name: "Информация о партии отсутствует"}
    ];

    this.$get = function(){
        return parties;
    };
});