'use strict';

/* groups */

appProviders.provider("groups", function groupProvider() {

    var groups = {
        '129': {
            color: 'green', //'#C0DC49'
            subgroups: [
                '7',
                '8',
                '18',
                '19',
                '20'
            ]
        },
        '130': {
            color: 'orange',  //'#FBF583',
            subgroups: [
                '3',
                '9',
                '15',
                '16',
                '17'
            ]
        },
        '131': {
            color: 'red', //'#FDBCAB',
            subgroups: [
                '1',
                '2',
                '12',
                '13',
                '14'
            ]
        },
        '132': {
            color: 'blue',  //'#B5E6ED',
            subgroups: [
                '4',
                '6',
                '10',
                '11',
                '21'
            ]
        },
        '133': {
            color: 'purple',  //'#FBC9E4',
            subgroups: [
                '5',
                '22',
                '23',
                '24',
                '25'
            ]
        },
    };

    this.$get = function(){
        return groups;
    };
});