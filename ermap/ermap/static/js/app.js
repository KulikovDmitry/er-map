'use strict';

var narrotApp = angular.module("ermapApp", [
    'appControllers',
    'appServices',
    'appProviders',
    'appFilters',
    'matchMedia',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'ngMaterial',
    'ngAnimate',
    'ngFileUpload',
    'angular-carousel'
    ]);


narrotApp.config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.months = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
    ];

    $mdDateLocaleProvider.shortMonths = [
        'Янв',
        'Фев',
        'Мар',
        'Апр',
        'Май',
        'Июн',
        'Июл',
        'Авг',
        'Сен',
        'Окт',
        'Ноя',
        'Дек'
    ];

    $mdDateLocaleProvider.days = [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
    ];
    
    $mdDateLocaleProvider.shortDays = [
        'Вс',
        'Пн',
        'Вт',
        'Ср',
        'Чт',
        'Пт',
        'Сб'
    ];
    
    // Can change week display to start on Monday.
    $mdDateLocaleProvider.firstDayOfWeek = 1;

    // Optional.
    //$mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14,15,16,17,18,19,
    //                               20,21,22,23,24,25,26,27,28,29,30,31];
    // In addition to date display, date components also need localized messages
    // for aria-labels for screen-reader users.

    $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
        return 'Неделя ' + weekNumber;
    };

    $mdDateLocaleProvider.msgCalendar = 'Календарь';

    $mdDateLocaleProvider.msgOpenCalendar = 'Открыть календарь';
});


narrotApp.config(function($mdIconProvider) {
    $mdIconProvider.fontSet('fa', 'fontawesome');
});