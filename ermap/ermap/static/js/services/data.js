'use strict';

/* dataService */

appServices.service("dataService", function($rootScope, $http, $q, $interval, filtersService) {
    var _last_updated = null;
    // Данные
    var _data = {};

    var parse_data = function(data) {
        var objects = {}
        angular.forEach(data, function(obj) {
            obj.images = obj.images ? obj.images : [],
            obj.color = obj.color ? obj.color : 'white',
            obj.status = obj.status ? obj.status : 'not_active'
            objects[obj.id] = obj;
        });
        return objects;
    };

    // Обновление состояния на сервере
    var updateState = function(data) {
        var deferred = $q.defer();
        $http.post("/update", data).then(
            function successCallback(response) {
                // Всегда возвращается полный объект
                data = response.data.data;
                var id = data.id;
                _data[id] = data;

                var changed = {};
                changed[id] = data;

                $rootScope.$broadcast(
                    "data:changed",
                    {
                        data: changed,
                        full_update: false
                //         data: filtersService.filter(_data),
                //         full_update: true
                    }
                );
                deferred.resolve(data);
            }, function errorCallback(response) {
                deferred.reject("Update data error")
            }
        );
        return deferred.promise;
    };

    // Загружает актуальные данные с сервера
    var load_data = function() {
        console.log("Loading data...");
        $http.get("/data").then(
            function successCallback(response) {
                _data = parse_data(response.data.data);
                $rootScope.$broadcast(
                    "data:changed",
                    {
                        data: filtersService.filter(_data),
                        full_update: true
                    }
                );
            },
            function errorCallback(response) {
                $rootScope.$broadcast("alert.error", "Ошибка при получении данных");
            }
        );
    };

    // Обновление фильтров
    $rootScope.$on("filter.update", function(event, args) {
        console.log("filter update");
        $rootScope.$broadcast(
            "data:changed",
            {
                data: filtersService.filter(_data),
                full_update: true
            }
        );
    });

    // Добавляет новый объект
    this.add = function (data) {
        return updateState(data);
    };

    // Обновляет существующий объект
    this.update = function (data) {
        return updateState(data);
    };

    // Возвращает актуальные события (соответствующие текущему фильтру)
    this.get_actual_data = function () {
        return filtersService.filter(_data);
    };

    // Возвращает событие по уникальному идентификатору
    this.get_data = function (id) {
        return _data[id];
    };

    load_data();
    // Обновление данных 1 раз в минуту
    $interval(function() {
        load_data();
        }, 60*1000);
});