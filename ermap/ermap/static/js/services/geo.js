'use strict';

/* geoService */

appServices.service("geoService", function($http) {
    var _last_updated = null;

    // Загружает GeoJSON файл с сервера
    this.load_geojson = function(region_id, reverse) {
        var suffix = '';
        if (reverse) {
            suffix = '_reverse';
        }
        var filename = "region_" + region_id + suffix + ".geojson";
        console.log("Loading file " + filename);
        return $http.get("/geojson/" + filename);
    };
});