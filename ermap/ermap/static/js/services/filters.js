'use strict';

/* filtersService */

appServices.service("filtersService", function($rootScope) {

    // Параметры фильтрации
    var _params = {
        date: {},
        colors: {}
    };

    var filter_by_date = function(object) {
        var date = new Date(object.published_date);

        if (_params.date.range == 'all') {
            return true;
        }

        if (_params.date.range == 'today') {
            var begin = moment().startOf('day');
            var end = moment().endOf('day');
        }

        if (_params.date.range == 'yesterday') {
            var begin = moment().subtract(1, 'day').startOf('day');
            var end = moment().subtract(1, 'day').endOf('day');
        }

        if (_params.date.range == 'last_week') {
            var begin = moment().subtract(6, 'days').startOf('day');
            var end = moment().endOf('day');
        }

        if (_params.date.range == 'last_month') {
            var begin = moment().subtract(29, 'days').startOf('day');
            var end = moment().endOf('day');
        }

        if (_params.date.range == 'custom') {
            var begin = _params.date.custom.after;
            var end = _params.date.custom.before;
        };

        // ТОDO: необходимо кэшировать range, чтобы избежать пересчета для каждого события
        var range = new DateRange(begin, end);
        return range.contains(date);
    };

    var filter_by_color = function(object) {
        if (object.color == "red" && _params.colors.red) {return true;};
        if (object.color == "yellow" && _params.colors.yellow) {return true;};
        if (object.color == "green" && _params.colors.green) {return true;};
        if (object.color != "red" &&
            object.color != "yellow" &&
            object.color != "green" &&
            _params.colors.other) {return true;};
        return false;
    };

    var filter_by_event_id = function(object) {
        if (!_params.event_id) {
            // Отсутствующее поле
            return true;
        }
        return Boolean(object.event_id === parseInt(_params.event_id));
    };

    var filter_by_election = function(object) {
        if (!object.election_type && (_params.election & (1<<30))) {
            // Отсутствующее поле
            return true;
        }
        return Boolean(object.election_type & _params.election);
    };

    var filter_by_party = function(object) {
        if (!object.party && (_params.party & (1<<30))) {
            // Отсутствующее поле
            return true;
        }
        return Boolean(object.party & _params.party);
    };

    var filter_by_group = function(object) {
        if (!object.group || object.group === "all") {
        //     // Отсутствующее поле
             return true;
        };
        var subgroup = object.group.split('.')[1];
        if (subgroup) {
            var subgroup_value = 1 << (parseInt(subgroup) - 1);
            return Boolean(subgroup_value & _params.groups);
        }
        return false;
    };

    var filter_by_type = function(object) {
        if (!object.type && (_params.types.indexOf('0') != -1 )) {
            // Отсутствующее поле
            return true;
        }
        var type = object.type ? object.type.toString() : ''
        return _params.types.indexOf(type) != -1;
    };

    var filter_by_subtype = function(object) {
        if (!object.subtype) {
            // Отсутствующее поле
            return true;
        }
        return _params.subtypes.indexOf(object.subtype) != -1;
    };

    /* Осуществляет фильтрацию данных */
    this.filter = function (data) {
        var result = {};
        angular.forEach(data, function(object, id) {
            if (filter_by_color(object) && filter_by_date(object) && filter_by_event_id(object) && filter_by_party(object) && filter_by_election(object) && filter_by_group(object) && filter_by_type(object) && filter_by_subtype(object)) {
                result[id] = object;
            };
        });
        return result;
    };

    // Обновляет параметры фильтрации
    this.update = function (params) {
        _params = params;
        $rootScope.$broadcast("filter.update", {});
    };
});