'use strict';

/* userService */

appServices.service("userService", function() {
    // Текущий пользователь
    var username_element = angular.element(document.querySelector('#username'))[0];
    var username = username_element.innerText || username_element.innerHTML;
    console.log("current user: " + username);

    var groupname_element = angular.element(document.querySelector('#group'))[0];
    var groupname = groupname_element.innerText || groupname_element.innerHTML;
    console.log("current group: " + groupname);

    var role_element = angular.element(document.querySelector('#role'))[0];
    var role = role_element.innerText || role_element.innerHTML;
    console.log("current role: " + role);

    var groups = {
        '129': {color: 'green'}, //'#C0DC49'},
        '130': {color: 'orange'}, //'#FBF583'},
        '131': {color: 'red'}, //'#FDBCAB'},
        '132': {color: 'blue'}, //'#B5E6ED'},
        '133': {color: 'purple'}, //'#FBC9E4'},
    };

    var permissions = {};
    // Создание нового события
    permissions["event.create"] = ["owner",].indexOf(role) !== -1;
    // Задание и уточнение местоположения ЛЮБЫХ маркеров (без учета владения)
    permissions["event.location"] = ["owner", "tech"].indexOf(role) !== -1;
    // Обновление данных (без учета владения)
    permissions["event.update"] = ["owner", "tech"].indexOf(role) !== -1;
    // Задание описания (без учета владения)
    permissions["event.description"] = ["owner", "tech"].indexOf(role) !== -1;
    // Задание комментария
    permissions["event.comment"] = ["tech"].indexOf(role) !== -1;
    // Загрузка фото (без учета владения)
    permissions["event.photo"] = ["owner", "tech"].indexOf(role) !== -1;
    // Обновление статуса
    permissions["event.update_status"] = [].indexOf(role) !== -1;
    // Обновление цвета
    permissions["event.update_color"] = ["tech", ].indexOf(role) !== -1;
    // Генерация отчета
    permissions["report.create"] = [].indexOf(role) !== -1;

    console.log(permissions);

    this.get_username = function() {
        return username;
    };

    this.get_groupname = function() {
        return groupname;
    };

    this.is_owner = function(object) {
        // Владельцем может быть контроллер и технолог
        var group = object.group.split('.');
        return object.owner === username || group[0] === groupname || object.group === groupname;
    };

    this.get_color = function(object) {
        try {
            var group = object.group.split('.')[0];
            return groups[group].color;
        } catch (TypeError) {
            return "cadetblue";
        }
    }

    this.is_permit = function(object, permission) {
        try {
            if (role === 'superuser') {return true;};
            return permissions[permission] && ((object && Object.keys(object).length) ? this.is_owner(object) : true);
        } catch (TypeError) {
            console.log("Uknown permission " + permission);
            return false;
        }
    }
});