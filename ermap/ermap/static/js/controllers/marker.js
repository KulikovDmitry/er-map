'use strict';

/* MarkerController */

appControllers.controller("MarkerController", [
    '$rootScope',
    '$scope',
    '$http',
    '$window',
    '$timeout',
    'dataService',
    'userService',
    'parties',
    'groups',
    'elections',
    'Upload',
    function($rootScope,
        $scope,
        $http,
        $window,
        $timeout,
        dataService,
        userService,
        parties,
        groups,
        elections,
        Upload) {
        $scope.model = {};
        $scope.obj = {
            upload_images: [],
        };

        $scope.parties = parties;
        $scope.elections = elections;
        $scope.groups = groups;

        var changeDescPromise = null;
        var changeCommentPromise = null;

        // Слайды
        $scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        var slides = $scope.slides = [];
        var currIndex = 0;
        $scope.colors = ["#fc0003", "#f70008", "#f2000d", "#ed0012", "#e80017", "#e3001c", "#de0021", "#d90026", "#d4002b", "#cf0030", "#c90036", "#c4003b", "#bf0040", "#ba0045", "#b5004a", "#b0004f", "#ab0054", "#a60059", "#a1005e", "#9c0063", "#960069", "#91006e", "#8c0073", "#870078", "#82007d", "#7d0082", "#780087", "#73008c", "#6e0091", "#690096", "#63009c", "#5e00a1", "#5900a6", "#5400ab", "#4f00b0", "#4a00b5", "#4500ba", "#4000bf", "#3b00c4", "#3600c9", "#3000cf", "#2b00d4", "#2600d9", "#2100de", "#1c00e3", "#1700e8", "#1200ed", "#0d00f2", "#0800f7", "#0300fc"];

        var watchers = [];
        $rootScope.$watch('current', function() {
            if ($rootScope.current) {
                console.log($rootScope.current);

                angular.forEach(watchers, function(watcher) {
                    watcher();
                });
                $scope.model = angular.copy(dataService.get_data($rootScope.current));

                var published_date = $scope.model.published_date;
                var converted_published_date = published_date.toLocaleString();
                $scope.model.published_date = new Date(converted_published_date);

                $scope.slides = $scope.create_slides($scope.model.images);
                console.log("count slides:" + $scope.slides.length);

                if ($scope.model.group != "all") {
                    $scope.group1 = $scope.model.group.split('.')[0];
                    $scope.group2 = $scope.model.group.split('.')[1] || "";
                }

                watchers = [
                    $scope.$watch('model.group', on_group_changed),
                    $scope.$watch('model.color', on_color_changed),
                    $scope.$watch('model.election_type', on_election_type_changed),
                    $scope.$watch('model.party', on_party_changed),
                    $scope.$watch('model.type', on_type_changed),
                    $scope.$watch('model.subtype', on_subtype_changed),
                    $scope.$watch('model.desc', on_desc_changed),
                    $scope.$watch('model.comment', on_comment_changed),
                ];
            };
        });

        $scope.create_slides = function(images) {
            var slides = [];
            angular.forEach(images, function(image, i) {
                slides.push({
                    id: i,
                    label: 'slide #' + (i + 1),
                    origin: '/image/' + $scope.model.id + '/' + image.hash + '?download=1',
                    img: '/image/' + $scope.model.id + '/' + image.hash + '?width=480&height=360',
                    // color: $scope.colors[ (i*10) % $scope.colors.length],
                    // odd: (i % 2 === 0)
                });
            });
            return slides;
        };

        var addSlides = function(images) {
            var total = $scope.slides.length;

            angular.forEach(images, function(image, i) {
                $scope.slides.push({
                    id: total + i,
                    label: 'slide #' + ( total + i + 1),
                    origin: '/image/' + $scope.model.id + '/' + image.hash + '?download=1',
                    img: '/image/' + $scope.model.id + '/' + image.hash + '?width=480&height=360',
                });
            });
        };

        $scope.submit = function() {
            // Сохранение на сервере
            var data = {
                id: $scope.model.id,
                desc: $scope.model.desc,
            };
            dataService.update(data).then(
                function successCallback(response) {
                    $rootScope.$broadcast("alert.success", "Описание успешно сохранено");
                },
                function errorCallback(response) {
                    $rootScope.$broadcast("alert.error", "При сохранении описания произошла ошибка");
                }
            );

            // Загрузка изображений
            if ($scope.obj.upload_images.length) {
                $rootScope.$broadcast("alert.success", "Начинается загрузка файлов. Это может занять некоторое время");
                angular.forEach($scope.obj.upload_images, function(image) {
                    console.log("Uploading file " + image.name);
                    $scope.upload($scope.model.id, image);
                });
            }
            $rootScope.$broadcast("marker:closed", $scope.model.id);
        }

        $scope.cancel = function() {
            $rootScope.$broadcast("marker:closed", $scope.model.id);
        }

        $scope.open = function(url) {
            console.log("Opening url " + url);
            $window.open(url);
        }

        var on_desc_changed = function(newValue, oldValue) {
            if (typeof $scope.model.desc != 'undefined' && newValue !== oldValue) {
                if (changeDescPromise){
                    $timeout.cancel(changeDescPromise);
                }
                changeDescPromise = $timeout(function() {
                    if ($scope.model.id) {
                        var data = {
                            id: $scope.model.id,
                            desc: $scope.model.desc,
                        };
                        dataService.update(data).then(
                            function successCallback(response) {
                                $rootScope.$broadcast("alert.success", "Описание успешно сохранено");
                            },
                            function errorCallback(response) {
                                $rootScope.$broadcast("alert.error", "При сохранении описания произошла ошибка");
                            }
                        );
                    };
                }, 3000, function() {
                    changeDescPromise = null;
                });
            };
        };

        var on_comment_changed = function(newValue, oldValue) {
            if (typeof $scope.model.comment != 'undefined' && newValue !== oldValue) {
                if (changeCommentPromise){
                    $timeout.cancel(changeCommentPromise);
                }
                changeCommentPromise = $timeout(function() {
                    if ($scope.model.id) {
                        var data = {
                            id: $scope.model.id,
                            comment: $scope.model.comment,
                        };
                        dataService.update(data).then(
                            function successCallback(response) {
                                $rootScope.$broadcast("alert.success", "Комментарий успешно сохранен");
                            },
                            function errorCallback(response) {
                                $rootScope.$broadcast("alert.error", "При сохранении комментария произошла ошибка");
                            }
                        );
                    };
                }, 3000, function() {
                    changeCommentPromise = null;
                });
            };
        };

        $scope.$watch('group1', function() {
            if (typeof $scope.group1 != 'undefined') {
                $scope.model.group = $scope.group1 + '.' + $scope.group2;
            };
        });

        $scope.$watch('group2', function() {
            if (typeof $scope.group2 != 'undefined') {
                $scope.model.group = $scope.group1 + '.' + $scope.group2;
            };
        });

        $scope.$watch('obj.upload_images', function() {
            if ($scope.obj.upload_images.length) {
                $rootScope.$broadcast("alert.success", "Начинается загрузка файлов. Это может занять некоторое время");
                angular.forEach($scope.obj.upload_images, function(image) {
                    console.log("Uploading file " + image.name);
                    $scope.upload($scope.model.id, image);
                });
            };
        });

        var on_group_changed = function(newValue, oldValue) {
            if (typeof $scope.model.group != 'undefined' && newValue !== oldValue) {
                console.log("new model.group: " + newValue + " old: " + oldValue);
                dataService.update({
                    id: $scope.model.id,
                    group: $scope.model.group
                });
            };
        };

        var on_color_changed = function(newValue, oldValue) {
            if (typeof $scope.model.color != 'undefined' && newValue !== oldValue) {
                console.log("new model.color: " + newValue + " old: " + oldValue);
                dataService.update({
                    id: $scope.model.id,
                    color: $scope.model.color
                });
            };
        };

        var on_election_type_changed = function(newValue, oldValue) {
            if (typeof $scope.model.election_type != 'undefined' && newValue !== oldValue) {
                console.log("new model.election_type: " + newValue + " old: " + oldValue);
                dataService.update({
                    id: $scope.model.id,
                    election_type: $scope.model.election_type
                });
            };
        };

        var on_party_changed = function(newValue, oldValue) {
            if (typeof $scope.model.party != 'undefined' && newValue !== oldValue) {
                console.log("new model.party: " + newValue + " old: " + oldValue);
                dataService.update({
                    id: $scope.model.id,
                    party: $scope.model.party
                });
            };
        };

        var on_type_changed = function(newValue, oldValue) {
            if (typeof $scope.model.type != 'undefined' && newValue !== oldValue) {
                console.log("new model.type: " + newValue + " old: " + oldValue);
                dataService.update({
                    id: $scope.model.id,
                    type: $scope.model.type
                });
            };
        };

        var on_subtype_changed = function(newValue, oldValue) {
            if (typeof $scope.model.subtype != 'undefined' && newValue !== oldValue) {
                console.log("new model.subtype: " + newValue + " old: " + oldValue);
                dataService.update({
                    id: $scope.model.id,
                    subtype: $scope.model.subtype
                });
            };
        };

        $scope.isNotPermit = function(permission) {
            var is_permit = userService.is_permit($scope.model, permission);
            return !is_permit;
        };

        // Изменить статус ('active', 'not_active')
        $scope.setStatus = function(status) {
            dataService.update({
                id: $scope.model.id,
                status: status,
            }).then(
                function successCallback(data) {
                    $rootScope.$broadcast(
                        "alert.success",
                        status == "active" ? "Маркер стал активным. Для доступа к нему включите слой \"Активные\"" : "Маркер стал неактивным. Для доступа к нему включите слой \"Удаленные\""
                    );
                }
            );
            // $rootScope.current.leafletObject.closePopup();
        }

        // Изменить тип выборов
        $scope.setElectionType = function(election_type) {
            if ($scope.isNotPermit('event.update')) {
                return;
            };
            console.log("setElectionType before: " + election_type);
            // election_type = $scope[election_type];
            // console.log("setElectionType after: " + election_type);

            if ($scope.model.election_type == 1) {
                $scope.model.election_type = election_type;
            }
            else if ($scope.model.election_type == 2) {
                if (election_type == 2) {
                    $scope.model.election_type = null;
                };
                if (election_type == 4) {
                    $scope.model.election_type = 6;
                };
                if (election_type == 1) {
                    $scope.model.election_type = 1;
                };
            }
            else if ($scope.model.election_type == 4) {
                if (election_type == 2) {
                    $scope.model.election_type = 6;
                };
                if (election_type == 4) {
                    $scope.model.election_type = null;
                };
                if (election_type == 1) {
                    $scope.model.election_type = 1;
                };
            }
            else if ($scope.model.election_type == 6) {
                if (election_type == 2) {
                    $scope.model.election_type = 4;
                };
                if (election_type == 4) {
                    $scope.model.election_type = 2;
                };
                if (election_type == 1) {
                    $scope.model.election_type = 1;
                };
            }
            else if ($scope.model.election_type == 1) {
                if (election_type == 2) {
                    $scope.model.election_type = 2;
                };
                if (election_type == 4) {
                    $scope.model.election_type = 4;
                };
                if (election_type == 1) {
                    $scope.model.election_type = null;
                }
            }
            else {
                $scope.model.election_type = election_type;
            };
            console.log("setElectionType: " + $scope.model.election_type);
        };

        $scope.isElectionType = function(election_type) {
            if ($scope.model.election_type === election_type) {
                return true;
            };
            if ($scope.model.election_type === 6) {
                if (election_type === 2) {
                    return true;
                };
                if (election_type === 4) {
                    return true;
                };
            };
            return false;
        };

        // Изменить тип нарушения
        $scope.setType = function(type, empty) {
            if ($scope.isType(type)) {
                $scope.model.type = null;
            }
            else {
                $scope.model.type = type;
                if (empty) {
                    $scope.model.subtype = null;
                };
            };
            console.log("setType: " + $scope.model.type);
        };

        $scope.isType = function(type) {
            return $scope.model.type === type;
        };

        // upload on file select or drop
        $scope.upload = function (id, file) {
            Upload.upload({
                url: '/upload',
                data: {
                    file: file
                }
            }).then(
                function successCallback(response) {
                    var data = {
                        id: id,
                        images: response.data.images,
                    };
                    dataService.update(data).then(
                        function successCallback(data) {
                            addSlides(response.data.images);
                            $rootScope.$broadcast("alert.success", "Файл " + file.name + " успешно загружен");
                        },
                        function errorCallback(response) {
                            $rootScope.$broadcast("alert.error", "При загрузке файла " + file.name + " произошла ошибка");
                        }
                    )
                },
                function errorCallback(response) {
                    console.log('Error while uploading image: ' + response.status);
                    $rootScope.$broadcast("alert.error", "При загрузке файла " + file.name + " произошла ошибка");
                },
                function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                }
            );
        };
    }
]);