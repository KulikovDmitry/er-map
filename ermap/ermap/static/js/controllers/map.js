'use strict';

/* mapController */

appControllers.controller("mapController", [
    "$rootScope",
    "$scope",
    "$timeout",
    "leafletData",
    "screenSize",
    "dataService",
    "userService",
    "geoService",
    function(
        $rootScope,
        $scope,
        $timeout,
        leafletData,
        screenSize,
        dataService,
        userService,
        geoService
    ) {

    var sidebar = new L.control.sidebar('sidebar');

    var addMarkerButton = L.easyButton({
        id: 'addMarkerButton',
        position: 'bottomright',
        type: 'replace',
        leafletClasses: true,
        states: [{
            stateName: 'add-marker',
            onClick: function(button, map) {
                console.log('Map is centered at: ' + map.getCenter().toString());
                var point = map.getCenter();
                addMarker(point.lat, point.lng);
            },
            title: 'Установить новый маркер в центр экрана',
            icon: 'fa fa-map-marker'
        }]
    });

    $scope.mouseoverPromise = null;
    $scope.current_open_popup = null;

    angular.extend($scope, {
        region_id: '52',
        nnovgorod: {
            lat: 56.32689,
            lng: 44.005986,
            zoom: 8
        },
        maxbounds: {
            northEast: {
                lat: 59.00,
                lng: 48.00
            },
            southWest: {
                lat: 54.00,
                lng: 40.50
            }
        },
        markers: {},
        layers: {
            baselayers: {
                yandex: {
                    name: 'Яндекс',
                    type: 'yandex',
                    layerOptions: {
                        layerType: 'map',
                        traffic: false,
                        continuousWorld: false
                    }
                },
                google: {
                    name: 'Google',
                    layerType: 'ROADMAP', // 'TERRAIN', 'HYBRID', 'ROADMAP'
                    type: 'google',
                    layerOptions: {
                        continuousWorld: false
                    }
                },
                osm: {
                    name: 'OpenStreetMap',
                    url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    type: 'xyz',
                    layerOptions: {
                        continuousWorld: false
                    }
                }
            },
            overlays: {
                active: {
                    name: "Активные",
                    type: "group", // "markercluster", "group"
                    visible: true,
                    layerOptions: {
                        showCoverageOnHover: false,
                        removeOutsideVisibleBounds: true
                    },
                },
                not_active: {
                    name: "Удаленные",
                    type: "group",
                    visible: false,
                },
                // Слой маркеров, не попадающих под условие фильтрации
                hidden: {
                    name: "",
                    type: "group",
                    visible: false,
                    layerParams: {
                        showOnSelector: false,
                    }
                }
            }
        },
        geojson: {},
        awesomeMarkerIcon: {
            type: 'awesomeMarker',
            icon: 'circle',
            prefix: 'fa',
            markerColor: 'blue',
            iconColor: 'white'
        },
        controls: {
            custom: [
                sidebar,

                // Кнопка для добавления маркера
                addMarkerButton,
                // Контрол для определения текущего местоположения
                new L.control.locate({
                    position: 'bottomright',  // set the location of the control
                    // layer: new L.LayerGroup(),  // use your own layer for the location marker
                    drawCircle: true,  // controls whether a circle is drawn that shows the uncertainty about the location
                    // follow: true,  // follow the user's location
                    setView: true, // automatically sets the map view to the user's location, enabled if `follow` is true
                    keepCurrentZoomLevel: false, // keep the current map zoom level when displaying the user's location. (if `false`, use maxZoom)
                    // stopFollowingOnDrag: false, // stop following when the map is dragged if `follow` is true (deprecated, see below)
                    // remainActive: false, // if true locate control remains active on click even if the user's location is in view.
                    markerClass: L.circleMarker, // L.circleMarker or L.marker
                    circleStyle: {
                        fillOpacity: 0.05,
                        opacity: 0.05
                    },  // change the style of the circle around the user's location
                    // markerStyle: {},
                    // followCircleStyle: {},  // set difference for the style of the circle around the user's location while following
                    // followMarkerStyle: {},
                    icon: 'fa fa-crosshairs',  // class for icon, fa-location-arrow or fa-map-marker
                    // iconLoading: 'fa fa-spinner fa-spin',  // class for loading icon
                    // iconElementTag: 'span',  // tag for the icon element, span or i
                    // circlePadding: [0, 0], // padding around accuracy circle, value is passed to setBounds
                    // metric: true,  // use metric or imperial units
                    // onLocationError: function(err) {
                    //     alert(err.message)
                    // },  // define an error callback function
                    // onLocationOutsideMapBounds:  function(context) { // called when outside map boundaries
                    //     alert(context.options.strings.outsideMapBoundsMsg);
                    // },
                    // showPopup: true, // display a popup when the user click on the inner marker
                    strings: {
                        title: "Моё местоположение",  // title of the locate control
                    //     metersUnit: "meters", // string for metric units
                    //     feetUnit: "feet", // string for imperial units
                        popup: "Вы находитесь в радиусе {distance} м. от данной точки",  // text to appear if user clicks on circle
                        outsideMapBoundsMsg: "Ваше местоположение определено вне границ карты" // default message for onLocationOutsideMapBounds
                    },
                    locateOptions: {
                        maxZoom: 18
                    }
                })
            ],
        },
        events: {
            map: {
                enable: ['zoomend', 'dblclick'],
                logic: 'emit'
            },
            geojson: {
            },
            markers: {
                enable: ['mouseover', 'mouseout', 'click', 'dragend', ],
                logic: 'emit'
            }
        },
        defaults: {
            zoomControlPosition: 'bottomright',
            tileLayerOptions: {
                opacity: 1.0,
                detectRetina: true,
                reuseTiles: true,
            },
            scrollWheelZoom: true,
            minZoom: 6,
            zoomAnimation: true,
            touchZoom: true,
            markerZoomAnimation: true,
            doubleClickZoom: false,
            fadeAnimation: true
        }
    });

    $scope.isMobile = screenSize.on('xs', function(isMatch) {
        $scope.isMobile = isMatch;
        console.log("isMobile:" + $scope.isMobile);
    });

    var addMarker = function(latitude, longitude) {
        if (userService.is_permit(null, "event.create")) {
            var data = {
                lat: latitude,
                lng: longitude,
                desc: "",
                status: "active",           // Текущий статус
                color: 'white',             // Цвет кружка внутри маркера
                owner: userService.get_username(), // Владелец маркера
                group: userService.get_groupname(), // Избирательный округ
                images: [],
            };

            dataService.add(data).then(
                function successCallback(data) {
                    var marker_id = data.id.split('-').join('_');
                    $scope.markers[marker_id] = createMarker(data);
                },
                function errorCallback(error) {
                    $rootScope.$broadcast("alert.error", "Ошибка при добавлении данных");
                }
            );
        };
    }

    $scope.$on("leafletDirectiveMap.zoomend", function(event, args) {
        var zoom = args.leafletObject.getZoom();
        if (zoom > 15) {
            addMarkerButton.enable();
        } else {
            addMarkerButton.disable();
        };
    });

    $scope.$on("leafletDirectiveMap.dblclick", function(event, args) {
        if (userService.is_permit(null, "event.create")) {
            $rootScope.$broadcast(
                "alert.info",
                "Добавить новое событие можно с помощью кнопки-маркера в правом нижнем углу"
            );
        };
    });

    var popup = function(object, id) {
        // var message = dataService.get_description(id);
            // if (typeof message == "undefined" || message.length == 0) {
                // message = "Нажмите, чтобы задать описание";
            // };
        $rootScope.current = id;
        $scope.current_open_popup = id;
        var message = "<div ng-include src=\"'partials/popup.html'\"></div>";
        object.bindPopup(message).openPopup(object);
    };

    $scope.$on("leafletDirectiveMarker.mouseover", function(event, args){
        args.leafletObject.dragging.disable();
        leafletData.getMap().then(function(map) {
            var zoom = map.getZoom();
            if (zoom > 15 && args.model.draggable) {
                args.leafletObject.dragging.enable();
            };
        });

        $scope.mouseoverPromise = $timeout(function() {
            popup(args.leafletObject, args.model.id);
        }, 333);
    });

    $scope.$on("leafletDirectiveMarker.mouseout", function(event, args) {
        console.log("cancel mouseover");
        args.leafletObject.dragging.enable();
        $timeout.cancel($scope.mouseoverPromise);
    });

    $scope.$on("leafletDirectiveMarker.click", function(event, args) {
        var id = args.leafletObject.options.id;
        if ($scope.isMobile && $scope.current_open_popup != id) {
            console.log("current_open_popup " + $scope.current_open_popup);
            console.log("open " + id);
            popup(args.leafletObject, args.model.id);
        } else {
            $rootScope.current = args.model.id;
            sidebar.open("description");
        };
    });

    $scope.$on("leafletDirectiveMarker.dragend", function(event, args) {
        dataService.update({
            id: args.model.id,
            lat: args.model.lat,
            lng: args.model.lng,
        });
    });

    $scope.$on("marker:closed", function(event, args) {
        leafletData.getMap().then(function(map) {
            map.closePopup();
        });
    });

    // Обновление всех данных
    $scope.$on("data:changed", function(event, args) {
        var data = args.data;
        var full_update = args.full_update;

        var markers = {};
        angular.forEach(data, function(object, id) {
            var marker_id = id.split('-').join('_');
            if (full_update) {
                markers[marker_id] = createMarker(object);
            }
            else {
                // Обновление одного маркера
                $scope.markers[marker_id] = createMarker(object);
            }
        });
        if (full_update) {
            $scope.markers = markers;
        }
    });

    // Создает маркер с заданными параметрами
    var createMarker = function(data) {
        var marker = {
            id: data.id,
            lat: data.lat,
            lng: data.lng,
            layer: data.status,
            icon: angular.copy($scope.awesomeMarkerIcon),
            opacity: data.status == "active" ? 1.0 : 0.3,
            draggable: userService.is_permit(data, "event.location")
        };
        marker.icon.iconColor = data.color;
        marker.icon.markerColor = userService.get_color(data);
        return marker;
    };

    // Загрузка подложки, выделяющей регион на фоне соседей
    geoService.load_geojson($scope.region_id, true).then(
        function successCallback(response) {
            var data = response.data;
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: {
                        weight: 5,
                        opacity: 0,
                        color: "#e0e0e0",
                        fillOpacity: 0.75,
                        fillColor: "#e0e0e0"
                    }
                }
            });
        }
    );
}]);
