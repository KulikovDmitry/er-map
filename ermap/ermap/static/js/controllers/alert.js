'use strict';

/* MarkerController */

appControllers.controller("alertController", [
    "$scope", function ($scope) {
        angular.extend($scope, {
            default_timeout: 10000,
        });

    $scope.alerts = [];

    $scope.addAlert = function(type, message, timeout) {
        var alert = {
            type: type,
            message: message
        };
        if (timeout) {
            alert.timeout = timeout;
        };
        $scope.alerts.push(alert);
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.$on("alert.success", function(event, args) {
        var message = args;
        // $scope.addAlert("success", message);
    });

    $scope.$on("alert.error", function(event, args) {
        var message = args;
        $scope.addAlert("danger", message);
    });

    $scope.$on("alert.warning", function(event, args) {
        var message = args;
        $scope.addAlert("warning", message);
    });

    $scope.$on("alert.info", function(event, args) {
        var message = args;
        $scope.addAlert("info", message);
    });
}]);