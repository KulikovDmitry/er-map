'use strict';

/* filterController */

appControllers.controller("filtersController", [
    "$scope",
    "filtersService",
    "parties",
    "elections",
    "groups",
    function ($scope, filtersService, parties, elections, groups) {
        $scope._filters = [];

        $scope.minDate = new Date(2016, 8, 1),  // 1 сентября 2016 года
        $scope.maxDate = new Date(2016, 9, 1),  // 1 октября 2016 года

        $scope.ranges = [
            { id: 0, value: 'all', name: 'За всё время'},
            { id: 1, value: 'today', name: 'Сегодня'},
            { id: 2, value: 'yesterday', name: 'Вчера'},
            { id: 3, value: 'last_week', name: 'За последнюю неделю'},
            { id: 4, value: 'last_month', name: 'За последний месяц'},
            { id: 5, value: 'custom', name: 'За заданный период'},
        ];

        // Типы выборов
        $scope.elections = angular.copy(elections);
        $scope.elections.push({
            value: 1 << 30,
            name: "Информация о выборах отсутствует"
        });
        var all_elections = (1 << 31) - 1;

        // Политические партии
        $scope.parties = angular.copy(parties);
        $scope.parties.push({
            value: 1 << 30,
            name: "Информация о партии отсутствует"
        });
        var all_parties = (1 << 31) - 1;

        // Избирательные округа
        $scope.groups = angular.copy(groups);
        var all_groups = (1 << 31) - 1;

        // Типы событий
        var all_types = [
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6'
        ];

        // Подтипы событий
        var all_subtypes = [
            '1-00',  // Не задан
            '1-01',
            '1-02',
            '1-03',
            '1-04',

            '2-00',  // Не задан
            '2-01',
            '2-02',
            '2-03',
            '2-04',
            '2-05',

            '5-00',  // Не задан
            '5-01',
            '5-02',
            '5-03',
            '5-04',
            '5-05',
        ];

        // Условия фильтрации
        $scope.params = {
            // Фильтрация по времени
            date :{
                range: 'custom',
                custom: {
                    after: $scope.minDate,
                    before: $scope.maxDate
                }
            },
            // Фильтрация по порядковому номеру события
            event_id: null,
            // Фильтрация по цвету кружка в маркере
            colors: {
                red: true,
                yellow: true,
                green: true,
                other: true
            },
            // Фильтрация по уровню выборов
            election: all_elections,
            // Фильтрация по политической партии
            party: all_parties,
            // Фильтрация по избирательным округам
            groups: all_groups,
            // Фильтрация по типам событий
            types: all_types,
            // Фильтрация по подтипам событий
            subtypes: all_subtypes,
        };

        // Изменяет бит на противоположный
        $scope.changeParty = function(value) {
            $scope.params.party = $scope.params.party ^ value;
        };

        $scope.isPartySet = function(value) {
            return Boolean($scope.params.party & value);
        };

        // Изменяет бит на противоположный
        $scope.changeElection = function(value) {
            $scope.params.election = $scope.params.election ^ value;
        };

        $scope.isElectionSet = function(value) {
            return Boolean($scope.params.election & value);
        };

        // Изменяет бит на противоположный
        $scope.changeGroup = function(value) {
            if (value in $scope.groups) {
                var subgroups = $scope.groups[value].subgroups;

                if ($scope.isGroupSet(value)) {
                    angular.forEach(subgroups, function(subgroup) {
                        // Снять выделение
                        var subgroup_value = 1 << (parseInt(subgroup) - 1);
                        $scope.params.groups = $scope.params.groups ^ subgroup_value;
                    });
                } else {
                    angular.forEach(subgroups, function(subgroup) {
                        // Установить выделение
                        var subgroup_value = 1 << (parseInt(subgroup) - 1);
                        console.log("set " + subgroup);
                        $scope.params.groups = $scope.params.groups | subgroup_value;
                    });
                };
            } else {
                var group = 1 << (parseInt(value) - 1);
                $scope.params.groups = $scope.params.groups ^ group;
            };
        };

        $scope.isGroupSet = function(value) {
            if (value in $scope.groups) {
                var subgroups = $scope.groups[value].subgroups;
                var is_set_all = true;
                angular.forEach(subgroups, function(subgroup) {
                    if (!$scope.isGroupSet(subgroup)) {
                        // Если не установлен ни один участок
                        is_set_all = false;
                    };
                });
                return is_set_all;
            } else {
                var group = 1 << (parseInt(value) - 1);
                return Boolean($scope.params.groups & group);
            };
        };

        $scope.changeType = function(value) {
            var index = $scope.params.types.indexOf(value);
            if (index != -1) {
                $scope.params.types.splice(index, 1);
            } else {
                $scope.params.types.push(value);
            }
        };

        $scope.isTypeSet = function(value) {
            return Boolean($scope.params.types.indexOf(value) != -1);
        };

        $scope.changeSubtype = function(value) {
            var index = $scope.params.subtypes.indexOf(value);
            if (index != -1) {
                $scope.params.subtypes.splice(index, 1);
            } else {
                $scope.params.subtypes.push(value);
            }
        };

        $scope.isSubtypeSet = function(value) {
            return Boolean($scope.params.subtypes.indexOf(value) != -1);
        };

        // Вызывается каждый раз при обновлении условий фильтрации
        $scope.$watch('params', function() {
            filtersService.update($scope.params)
        },
        true);
    }
]);