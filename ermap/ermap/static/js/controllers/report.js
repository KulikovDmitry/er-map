'use strict';

/* reportController */

appControllers.controller("reportController", [
    "$scope",
    "$http",
    "$window",
    "dataService",
    "userService",
    function ($scope, $http, $window, dataService, userService) {
        $scope.createReport = function() {

            var uids = [];
            angular.forEach(dataService.get_actual_data(), function(data) {
                uids.push(data.id);
            });

            $http.post('/report', uids).then(
                function successCallback(response) {
                    $window.open("/report?uid=" + response.data.uid);
                },
                function errorCallback(response) {
                    console.log("error report");
                    console.log(response);
                })
        };

        $scope.isNotPermit = function(permission) {
            var is_permit = userService.is_permit({}, permission);
            return !is_permit;
        };
}]);