# -*- coding: utf-8 -*-
"""Public forms."""
import os

from flask_wtf import Form
from wtforms import PasswordField, StringField
from wtforms.validators import DataRequired


class LoginForm(Form):
    '''
    Форма для входа в систему
    '''
    username = StringField(
        u'Имя пользователя',
        validators=[
            DataRequired()
            ]
        )
    password = PasswordField(
        u'Пароль',
        validators=[
            DataRequired()
            ]
        )

    def __init__(self, *args, **kwargs):
        """Create instance."""
        super(LoginForm, self).__init__(*args, **kwargs)

        self.available_users = dict()
        try:
            users_filename = os.getenv("ERMAP_FILENAME_USERS") or '/var/local/ermap/users'
            with open(users_filename, mode="rt") as f:
                for s in f:
                    tokens = s.split(":")
                    username = tokens[0].strip().decode("utf-8")
                    password = tokens[1].strip().decode("utf-8") if len(tokens) > 1 else None
                    role = tokens[2].strip() if len(tokens) > 2 else None
                    group = tokens[3].strip() if len(tokens) > 3 else None

                    self.available_users[username] = dict(
                        password=password,
                        role=role,
                        group=group
                    )
        except IOError as error:
            print(error)

    def validate(self):
        '''
            Проверяет корректность заполнения формы
        '''
        initial_validation = super(LoginForm, self).validate()
        if not initial_validation:
            return False

        if self.username.data not in self.available_users:
            self.username.errors.append(u'Пользователь "{}" не зарегистрирован'.format(self.username.data))
            return False

        if self.available_users[self.username.data]['password'] is None:
            self.username.errors.append(u'Учетная запись пользователя "{}" не активирована'.format(self.username.data))
            return False

        if self.available_users[self.username.data]['password'] != self.password.data:
            self.password.errors.append(u'Задан неверный пароль')
            return False

        self.role = self.available_users[self.username.data]['role']
        self.group = self.available_users[self.username.data]['group']
        return True
