# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
import os
import hashlib
import json
import errno
import tempfile
from datetime import datetime
import uuid

from flask import g, Blueprint, flash, redirect, render_template, request, url_for, abort, jsonify, send_file
from flask_login import login_required, login_user, logout_user

import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError, RqlDriverError

from ermap.extensions import login_manager
from ermap.public.forms import LoginForm
from ermap.user.forms import RegisterForm
from ermap.user.models import User
from ermap.utils import flash_errors, gzipped

blueprint = Blueprint('public', __name__, static_folder='../static')

RETHINKDB_HOST = os.environ.get('RDB_HOST') or 'localhost'
RETHINKDB_PORT = os.environ.get('RDB_PORT') or 28015
RETHINKDB_DATABASE = os.environ.get('RDB_DBNAME') or 'ermap'


# The pattern we're using for managing database connections is to have **a connection per request**. 
# We're using Flask's `@app.before_request` and `@app.teardown_request` for 
# [opening a database connection](http://www.rethinkdb.com/api/python/connect/) and 
# [closing it](http://www.rethinkdb.com/api/python/close/) respectively.
@blueprint.before_request
def before_request():
    try:
        g.rdb_conn = r.connect(
            host=RETHINKDB_HOST,
            port=RETHINKDB_PORT,
            db=RETHINKDB_DATABASE
        )
    except RqlDriverError:
        abort(503, "No database connection could be established.")


@blueprint.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


@login_manager.user_loader
def load_user(user_id):
    """Load user by ID."""
    return User.get_by_id(int(user_id))


@blueprint.route('/', methods=['GET', 'POST'])
def home():
    """Home page."""
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate():
            return render_template('public/map.html', form=form)
        else:
            flash_errors(form)
    return render_template('public/login.html', form=form)


@blueprint.route('/partials/popup.html', methods=['GET', ])
def popup():
    """popup template"""
    return render_template('partials/popup.html')


@blueprint.route('/logout/')
@login_required
def logout():
    """Logout."""
    logout_user()
    flash('You are logged out.', 'info')
    return redirect(url_for('public.home'))


@blueprint.route('/register/', methods=['GET', 'POST'])
def register():
    """Register new user."""
    form = RegisterForm(request.form, csrf_enabled=False)
    if form.validate_on_submit():
        User.create(username=form.username.data, email=form.email.data, password=form.password.data, active=True)
        flash('Thank you for registering. You can now log in.', 'success')
        return redirect(url_for('public.home'))
    else:
        flash_errors(form)
    return render_template('public/register.html', form=form)


@blueprint.route('/about/')
def about():
    """About page."""
    form = LoginForm(request.form)
    return render_template('public/about.html', form=form)


@blueprint.route('/data', methods=['GET', ])
def data():
    '''
    Get markers
    '''
    return jsonify(
        data=list(r.table('markers').run(g.rdb_conn))
    )


@blueprint.route('/geojson/<fname>', methods=['GET', ])
@gzipped
def geojson(fname):
    '''
    Get markers
    '''
    try:
        filename = '/var/local/ermap/data/geojson/{}'.format(fname)
        with open(filename, mode='rt') as f:
            data = json.loads(f.read())
            return jsonify(data)
    except IOError as error:
        print(error)
        abort(404)


@blueprint.route('/update', methods=['POST', ])
def update():
    '''
    Update marker
    '''
    data = request.json
    if 'id' in data:

        # Обновление существующей записи
        data["updated_date"] = r.now()

        _id = data.pop('id')
        images = data.pop('images', None)
        if data:
            print("data: {}".format(data))
            status = r.table('markers').get(_id).update(data).run(g.rdb_conn)
            print("status load data: {}".format(status))
        if images:
            print("images: {}".format(images))
            for image in images:
                status = r.table('markers').get(_id).update(
                    {
                        "images": r.row["images"].default([]).set_insert(image)
                    }
                ).run(g.rdb_conn)
                print("status load image: {}".format(status))
    else:
        # Добавление новой записи
        assert data
        data["published_date"] = r.now()

        # Получение порядкового номера события
        max_event_id = r.table('markers').max(index='event_id').run(g.rdb_conn)['event_id']
        data["event_id"] = max_event_id + 1

        status = r.table('markers').insert(data).run(g.rdb_conn)
        if status['inserted'] == 1:
            _id = status['generated_keys'][0]

    data.update(dict(_id=_id))
    status = r.table('tranlist').insert(data).run(g.rdb_conn)
    assert status['errors'] == 0
    assert status['deleted'] == 0
    assert status['unchanged'] == 0
    assert status['skipped'] == 0
    assert status['replaced'] == 0
    assert status['inserted'] == 1

    data = r.table('markers').get(_id).run(g.rdb_conn)
    return jsonify(data=data)


@blueprint.route('/upload', methods=['POST', ])
def upload():
    '''
    Upload image
    '''
    images = list()
    for key, storage in request.files.items():
        try:
            h = hashlib.sha256()
            with tempfile.NamedTemporaryFile(delete=False) as tmp:
                while True:
                    chunk = storage.read()
                    if not chunk:
                        break
                    h.update(chunk)
                    tmp.write(chunk)
            hashsum = h.hexdigest()
            os.rename(
                tmp.name,
                "/var/local/ermap/images/{}".format(hashsum)
                )
            images.append(dict(
                hash=hashsum,
                filename=storage.filename,
                mimetype=storage.mimetype
                ))
        except Exception:
            pass
    return jsonify(dict(
        status="OK",
        images=images
    ))


@blueprint.route('/image/<object_id>/<hashsum>', methods=['GET', ])
def image(object_id, hashsum):
    '''
    Download image for object
    '''

    width = int(request.args.get('width', 0))
    height = int(request.args.get('height', 0))
    download = int(request.args.get('download', 0))

    images = r.table('markers').get(object_id)['images'].run(g.rdb_conn)
    for image in images:
        if image['hash'] == hashsum and 'mimetype' in image:

            filename = '/var/local/ermap/images/{}'.format(hashsum)
            if width or height:
                # Загрузка превью
                thumbnail_filename = filename + '.thumbnail.{}.{}'.format(width, height)
                try:
                    return send_file(thumbnail_filename, mimetype=image['mimetype'])
                except IOError as err:
                    if err.errno != 2:
                        raise

                # Генерация и сохранение превью
                from PIL import Image
                im = Image.open(filename)
                im.thumbnail((width, height), Image.ANTIALIAS)
                im.save(thumbnail_filename, im.format)
                filename = thumbnail_filename
            return send_file(
                filename,
                mimetype=image['mimetype'],
                as_attachment=bool(download),
                attachment_filename=image['filename'].encode("utf-8")
                )
    abort(404)


PARTIES = {
    1: "Другое",
    2: "Единая Россия",
    4: "КПРФ",
    8: "ЛДПР",
    16: "Справедливая Россия",
}


TYPES = {
    "1": "Наглядная агитация",
    "2": "Массовые мероприятия",
    "3": "Работа ЛОМов",
    "4": "Подкуп избирателей",
    "5": "Постоянные точки",
    "6": "Прочее",
}


SUBTYPES = {
    "1-01": "Билборды",
    "1-02": "Плакаты",
    "1-03": "Газеты и АПМ",
    "1-04": "Прочее",
    "2-01": "Встреча",
    "2-02": "Митинг",
    "2-03": "Праздник",
    "2-04": "Прочее",
    "2-05": "Пикет",
    "5-01": "Штаб",
    "5-02": "Склад",
    "5-03": "Место встречи с агитаторами",
    "5-04": "Место встречи с избирателями",
    "5-05": "Прочее"
}


@blueprint.route('/report', methods=['GET', 'POST'])
def report():
    '''
    Generate report in Excel format
    '''
    if request.method == "POST":
        # Генерация отчёта
        uids = request.data

        from openpyxl import Workbook
        wb = Workbook()
        ws = wb.active
        ws.title = u"События"

        def handler_election_type(election_type):
            if not election_type:
                return ""
            election_types = []
            if (election_type & 2):
                election_types.append(u"ГД")
            if (election_type & 4):
                election_types.append(u"ЗС")
            if (election_type == 1):
                election_types = [u"Неизвестно", ]
            return ", ".join(election_types)

        HANDLERS = [
            ("№", lambda event: str(event["event_id"])),
            ("Дата создания", lambda event: "{:%d.%m.%Y}".format(event["published_date"])),
            ("Автор сообщения", lambda event: event["owner"]),
            ("Типы выборов", lambda event: handler_election_type(event["election_type"])),
            ("Избирательный округ ГД", lambda event: event["group"].split(".")[0] if event["group"] != "all" else ""),
            ("Избирательный округ ЗС", lambda event: event["group"].split(".")[1] if event["group"] != "all" else ""),
            ("Политическая партия", lambda event: PARTIES.get(int(event["party"])), ""),
            ("Тип события", lambda event: TYPES.get(str(event["type"]), "")),
            ("Подтип события", lambda event: SUBTYPES.get(event["subtype"], "")),
            ("Описание", lambda event: event["desc"]),
            ("Комментарий технолога", lambda event: event["comment"]),
            ("Количество загруженных фотографий", lambda event: len(event["images"])),
        ]

        all_events = r.table('markers').filter({"status": "active"}).run(g.rdb_conn)
        actual_events = filter(lambda event: event['id'] in uids, all_events)

        for column, handler in enumerate(HANDLERS, 1):
            ws.cell(row=1, column=column, value=handler[0])

        for row, event in enumerate(actual_events, 2):
            for column, handler in enumerate(HANDLERS, 1):
                try:
                    ws.cell(row=row, column=column, value=handler[1](event))
                except KeyError:
                    pass
        report_uid = uuid.uuid4()
        wb.save('/var/local/ermap/reports/{}'.format(report_uid))
        return jsonify(dict(uid=report_uid))
    elif request.method == "GET":
        # Отправка отчёта
        report_uid = request.args['uid']
        now = datetime.now()
        if report_uid:
            return send_file(
                '/var/local/ermap/reports/{}'.format(report_uid),
                as_attachment=True,
                attachment_filename="report_{0:%Y%m%d}_{0:%H%M}.xlsx".format(now)
            )
        abort(404)
